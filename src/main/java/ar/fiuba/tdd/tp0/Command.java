package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by osboxes on 10/09/15.
 */
public interface Command {
    void runCommand(Stack st);
}
