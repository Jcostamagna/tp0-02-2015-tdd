package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by osboxes on 11/09/15.
 */
public class MultiCommand implements Command {

    Operation operation;

    public MultiCommand(Operation op) {
        operation = op;
    }

    @Override
    public void runCommand(Stack st) {
        st.pop(); //Saco el operador
        Stack aux = new Stack();
        //Doy vuelta la pila para invertir el orden (afecta al resultado)
        while (!st.empty()) {
            aux.push(Float.parseFloat((String) st.pop()));
        }
        float resultado = (float) aux.pop();
        while (!aux.empty()) {
            resultado = operation.operation(resultado, (float)aux.pop());
        }
        st.push(Float.toString(resultado));
    }
}
