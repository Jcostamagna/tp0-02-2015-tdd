package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by osboxes on 11/09/15.
 */
public class SimpleCommand implements Command{

    Operation operation;

    public SimpleCommand(Operation op) {
        operation = op;
    }

    @Override
    public void runCommand(Stack st) {
        st.pop(); //Saco el operador
        String n2 = (String) st.pop();
        String n1 = (String) st.pop();
        float resultado = operation.operation(Float.parseFloat(n1), Float.parseFloat(n2));
        st.push(Float.toString(resultado));
    }
}
