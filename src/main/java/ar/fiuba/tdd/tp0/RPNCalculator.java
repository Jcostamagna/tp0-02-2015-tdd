package ar.fiuba.tdd.tp0;



import java.util.*;


public class RPNCalculator {

    public float eval(String expression) throws IllegalArgumentException {

        String[] operadores;
        try {
            operadores = expression.split(" ");
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }

        Stack st = new Stack();

        Map<String, Command> methodMap = confMap();

        Command defaultCommand = (Stack stack) -> { } ;

        for (String string: operadores) {
            st.push(string);
            try {
                methodMap.getOrDefault(string, defaultCommand).runCommand(st);
            } catch (EmptyStackException e) {
                throw new IllegalArgumentException();
            }
        }


        return Float.parseFloat((String) st.pop());
    }

    private Map<String,Command> confMap() {
        Map<String,Command> methodMap = new HashMap<String,Command>();

        methodMap.put("+", new SimpleCommand((float n1,float n2) -> { return n1 + n2; }));

        methodMap.put("-", new SimpleCommand((float n1,float n2) -> { return n1 - n2; }));

        methodMap.put("*", new SimpleCommand((float n1,float n2) -> { return n1 * n2; }));

        methodMap.put("MOD", new SimpleCommand((float n1,float n2) -> { return n1 % n2; }));

        methodMap.put("/", new SimpleCommand((float n1,float n2) -> { return n1 / n2; }));

        methodMap.put("++", new MultiCommand((float n1,float n2) -> { return n1 + n2; }));

        methodMap.put("**", new MultiCommand((float n1,float n2) -> { return n1 * n2; }));

        methodMap.put("--", new MultiCommand((float n1,float n2) -> { return n1 - n2; }));

        methodMap.put("//", new MultiCommand((float n1,float n2) -> { return n1 / n2; }));

        return methodMap;
    }

}
