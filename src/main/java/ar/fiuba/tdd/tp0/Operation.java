package ar.fiuba.tdd.tp0;

/**
 * Created by osboxes on 11/09/15.
 */
public interface Operation {

    float operation(float n1, float n2);
}
